function ageChecker() {
    // Считать с помощью модельного окна браузера данные пользователя:
    // имя и возраст.
    let name, age;
    // Если пользователь не ввел имя
    while (!name) {
        name = prompt('What is your name?', name);
    }
    // Либо при вводе возраста указал не число - спросить имя и возраст заново
    // (при этом значением по умолчанию для каждой из переменных должна быть
    // введенная ранее информация).
    while (!age || isNaN(age)) {
        age = prompt('How old are you?', age);
    }
    // Виведем в консоль значения переменных.
    console.log(name);
    console.log(age);

    // Если возраст меньше 18 лет.
    if (age < 18) {
        // Показать на экране сообщение:
        // You are not allowed to visit this website.
        alert('You are not allowed to visit this website!');
    } else if (age >= 18 && age <= 22) {
        // Если возраст от 18 до 22 лет (включительно) - показать окно
        // со следующим сообщением:
        // Are you sure you want to continue? и кнопками Ok, Cancel.
        let isConitinue = confirm('Are you sure you want to continue?');
        if (isConitinue === true) {
            // Если пользователь нажал Ok, показать на экране сообщение:
            // Welcome, + имя пользователя.
            alert('Welcome, ' + name + '!!!');
        } else {
            // Если пользователь нажал Cancel, показать на экране сообщение:
            // You are not allowed to visit this website.
            alert('You are not allowed to visit this website!');
        }
    } else {
        // Если возраст больше 22 лет - показать на экране сообщение:
        // Welcome, + имя пользователя.
        alert('Welcome, ' + name + '!!!');
    }
}
ageChecker();
